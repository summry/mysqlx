from mysqlx import mapper, sql
from typing import Any, List, Tuple, Mapping, Sequence


@mapper(namespace='user')
def user_count(): int


@mapper(batch=True)
def batch_insert(args: Sequence[Tuple[str, int, str, int, float, float, float]]): int


@mapper()
def select_name(id: int): str


@mapper()
def user_update(name: str, id: int): int


@mapper()
def user_update2(id: int, name: str): int


@mapper()
def select_all(): list[Mapping]


@mapper(sql_id='select_all')
def select_all2(): list[tuple]


@sql('select * from user where name = ?')
def select_users(name: str): list
