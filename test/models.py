from decimal import Decimal
from datetime import date, datetime
from mysqlx.orm import Model, KeyStrategy


class BaseModel(Model):
    __key__ = 'id'
    __del_flag__ = 'del_flag'
    __update_by__ = 'update_by'
    __update_time__ = 'update_time'
    __key_strategy__ = KeyStrategy.DB_AUTO_INCREMENT

    def __init__(self, id: int = None, create_by: int = None, create_time: datetime = None, update_by: int = None, update_time: datetime = None, del_flag: int = None): 
        self.id = id 
        self.create_by = create_by 
        self.create_time = create_time 
        self.update_by = update_by 
        self.update_time = update_time 
        self.del_flag = del_flag 


class User(BaseModel):
    __table__ = 'user'

    def __init__(self, age: int = None, birth_date: date = None, create_by: int = None, create_time: datetime = None, del_flag: int = None, grade: float = None, id: int = None, money: Decimal = None, name: str = None, point: float = None, sex: int = None, update_by: int = None, update_time: datetime = None):
        super().__init__(create_by=create_by, create_time=create_time, del_flag=del_flag, id=id, update_by=update_by, update_time=update_time) 
        self.age = age 
        self.birth_date = birth_date 
        self.grade = grade 
        self.money = money 
        self.name = name 
        self.point = point 
        self.sex = sex 

