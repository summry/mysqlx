from mysqlx import init_db, db


def full_test():
    db.sql('INSERT INTO person(name,age) VALUES(?,?)').execute('lisi', 26)

    print(db.sql('select * from person').select())
    print(db.sql('select * from person').select_one())

    print(db.sql('select id, name, age from person').query())
    print(db.sql('select id, name, age from person where name = ?').query_one('lisi'))
    print(db.sql('select id, name, age from person where name = :name').query_one(name='lisi'))

    print(db.sql('select count(1) from person').get())

    for u in db.sql('select * from person').page(1, 4).query():
        print(u)

    for u in db.sql('select * from person').page(1, 4).select():
        print(u)


if __name__ == '__main__':
    from config import SQLITE_CONF
    
    init_db('/Users/summy/project/python/sqlx-exec/test/test.db', **SQLITE_CONF)
    full_test()
