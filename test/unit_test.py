import unittest
from mysqlx import db, init_db
from config import DB_CONF
from db_test import full_test as db_test
from dbx_test import full_test as dbx_test
from mapper_test import full_test as mapper_test
from SqlExec_test import full_test as SqlExec
from dbx_sqlid_test import full_test as sqlid_test


class MyTestCase(unittest.TestCase):

    init_db(**DB_CONF)

    def test_db(self):
        db_test()
        self.assertEqual(True, True)

    def test_dbx(self):
        dbx_test()
        self.assertEqual(True, True)

    def test_mapper(self):
        mapper_test()
        self.assertEqual(True, True)

    def test_sqlexec(self):
        SqlExec()
        self.assertEqual(True, True)

    def test_sqlidexec(self):
        sqlid_test()
        self.assertEqual(True, True)


if __name__ == '__main__':
    unittest.main()
