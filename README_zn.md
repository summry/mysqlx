建议使用Python3.7及以上版本，因为mysqlx可以使用mysql-connector, 而mysql-connector用到的Cython回报 SyntaxError: future feature annotations is not defined错误。
当然如果不需要使用mysql-connector的Cython，可以使用低版本Python，这需要用户自己适配。

##### 报错原因：
1. 使用Cython版本过低

    https://github.com/cython/cython/issues/2950#issuecomment-679136993
2. 使用python3.7以下版本

   报错: https://stackoverflow.com/questions/52889746/cant-import-annotations-from-future/52890129

    根据PEP-563在py3.7中才能使用

    https://www.python.org/dev/peps/pep-0563/#enabling-the-future-behavior-in-python-3-7

##### 报错解决

1.升级Cython, 使用3.0版本
```
pip3.7 install Cython==3.0a1
```
2.使用python3.7以上版本

##### Install
```
pip install mysqlx
```
##### Usage Sample
```
CREATE TABLE `user` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `age` int NOT NULL,
  `birth_date` date DEFAULT NULL,
  `sex` tinyint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci


----------------------------------------------------------------------------------------

from mysqlx import db

db_conf = {
    'host': '127.0.0.1',
    'port': 3306, 
    'user': 'root', 
    'password': 'xxx', 
    'database': 'test',
    'pool_size': 5,
    'show_sql': True
}

if __name__ == '__main__':
    init_db(**db_conf)
    
    # Return effect rowcount
    rowcount = db.insert('user', name='张三', age=55, birth_date='1968-10-08', sex=0, grade=1.0, point=20.5, money=854.56)
    assert rowcount == 1, '1 effect rowcount'
    assert db.get('select count(1) from user') == 1, 'count is 1'

    # Return primary key
    id2 = db.save('user', name='李四', age=55, birth_date='1968-10-08', sex=0, grade=1.0, point=20.5, money=854.56)
    assert db.get('select count(1) from user') == 2, 'count is 2'

    db.execute('update user set name=? where id=?', '王五', id2)
    assert db.get('select name from user where id=?', id2) == '王五', 'execute'

    db.execute('update user set name=:name where id=:id', name='赵六', id=id2)
    assert db.select_one('select id, name from user where id=:id', id=id2)[0] == id2, 'execute'

    db.execute('update user set name=:name where id=:id', name='赵六', id=id2)
    assert db.query_one('select name from user where id=:id', id=id2)['name'] == '赵六', 'execute'

    args = [
        ('张三', 55, '1968-10-08', 0, 1.0, 20.5, 854.56),
        ('张三', 55, '1968-10-08', 0, 1.0, 20.5, 854.56)
    ]
    db.batch_execute('insert into user(name, age, birth_date, sex, grade, point, money) values(?,?,?,?,?,?,?)', args)
    users = db.select('select id, del_flag from user')
    assert len(users) == 4, 'batch_execute'
    users = db.query('select id, del_flag from user')
    assert len(users) == 4, 'batch_execute'

    users = db.select('select id, del_flag from user where id=?', id2)
    assert len(users) == 1, 'select'
    users = db.query('select id, del_flag from user where id=?', id2)
    assert len(users) == 1, 'select'

    users = db.select('select id, del_flag from user where id=:id', id=id2)
    assert len(users) == 1, 'select'
    users = db.query('select id, del_flag from user where id=:id', id=id2)
    assert len(users) == 1, 'select'

    db.execute('delete from user where id=? limit 1', id2)
    assert db.get('select count(1) from user') == 3, 'execute delete'
```
##### Transaction
```
@trans
def test_transaction():
    db.insert('user', name='张三', age=55, birth_date='1968-10-08', sex=0, grade=1.0, point=20.5, money=854.56)
    db.insert('user', name='李四', age=55, birth_date='1968-10-08', sex=0, grade=1.0, point=20.5, money=854.56)


def test_transaction2():
    with trans():
        db.insert('user', name='张三', age=55, birth_date='1968-10-08', sex=0, grade=1.0, point=20.5, money=854.56)
        db.insert('user', name='李四', age=55, birth_date='1968-10-08', sex=0, grade=1.0, point=20.5, money=854.56)
```
##### Note: the functions return type
```
get: Return only one object, like count
query_one: Return one row with dict
select_one: Return one row with tuple
find_by_id: Return one row with class instance object
query: Return list of dict
select: Return list of tuple
find: Return list of class instance object
```