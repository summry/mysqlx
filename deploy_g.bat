@echo off

set folder=deploy
set source=mysqlx

IF EXIST %folder% (
    echo 'exist---------.'
    for /r "%folder%" %%i in (*) do (
        del "%%i" /f /q >nul 2>nul
    )
) ELSE (
    echo 'not exist========.'
    md %folder%\%source%
)

copy "setup_g.py" %folder%\"setup.py"
copy "README_g.rst" %folder%\"README.rst"

for %%i in ("__init__.py", "generator.py", "generator.tpl") do (
    copy %source%\%%i %folder%\%source%
)

cd %folder%
python setup.py sdist
twine upload dist/*

