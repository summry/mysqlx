@echo off

set folder=deploy
set source=mysqlx

IF EXIST %folder% (
    echo 'exist---------.'
    for /r "%folder%" %%i in (*) do (
        del "%%i" /f /q >nul 2>nul
    )
) ELSE (
    echo 'not exist========.'
    md %folder%\%source%
)

for %%i in ("setup.py", "README.rst") do (
    copy %%i %folder%
)

for /f "usebackq delims=" %%i in (`dir "%source%\*.py" /b ^| findstr /v /x "generator.*"`) do (
    copy %source%\%%i %folder%\%source%
)

cd %folder%
python setup.py sdist
twine upload dist/*

