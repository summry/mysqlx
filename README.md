MySqlx是Python操作MySQL数据库的框架，主打的就是简单易用，容易上手，代码简洁，性能极高，因为对数据库驱动只做了一层薄薄的封装，以1.5.6版本为例，整个安装包只有19kb。MySqlx提供了多种操作数据库的方式：

- 类似MyBatis接口调用执行SQL，SQL与代码分离，接口可复用。
- 类似iBatis根据sql_id执行SQL，提供了更多操作数据库的函数，例如分页查询等。

- 可以直接执行SQL语句。

- ORM方式操作数据库。



##### 安装

如果网速较慢，也可以指定国内镜像安装

```shell
# pip install mysqlx
或
# pip install mysqlx -i https://mirrors.aliyun.com/pypi/simple
Collecting mysqlx
  Downloading mysqlx-1.5.6.tar.gz (19 kB)
```
Mapper文件
你可以在mapper文件夹下创建user_mapper.xml文件，如下：
```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "https://gitee.com/summry/mysqlx/blob/master/dtd/mapper.dtd">
<mapper namespace="user">
	<select id="select_all">
	   select id, name, age from user
	</select>
	
	<select id="select_by_name">
	   select id, name, age from user where name = ?
	</select>
    
	<select id="select_by_name2">
	   select id, name, age from user where name = :name
	</select>
    
	<select id="select_include" include="select_all">
	   {{ select_all }}
	     {% if name -%}
		  where name = :name
    	 {%- endif -%}
	</select>
</mapper>
```


##### 使用例子

```
from mysqlx.orm import Model
from typing import List, Tuple, Mapping
from mysqlx import mapper, sql, db, dbx, init_db

@mapper(namespace='user')
def select_all(): List

@mapper(namespace='user')
def select_by_name(name: str): List

@mapper(namespace='user')
def select_by_name2(name: str): List

@mapper(namespace='user')
def select_include(name: str): List

@sql('select id, name, age from user where name = ?')
def query_by_name(name: str): List(Mapping)

@sql('select id, name, age from user where name = :name')
def query_by_name2(name: str): List(Mapping)

if __name__ == '__main__':
    init_db(host='127.0.0.1', port='3306', user='xxx', password='xxx', database='test', pool_size=5, show_sql=True, mapper_path='./mapper')
    
    users = select_all()
    # result:
    # (3, 'zhangsan', 15)
    # (4, 'lisi', 26)
    # (5, 'wangwu', 38)
    
    users = select_by_name('zhangsan')
    # result:
    # (3, 'zhangsan', 15)
    
    users = select_by_name2(name='zhangsan')
    # result:
    # (3, 'zhangsan', 15)
    
    users = select_include(name='zhangsan')
    # result:
    # (3, 'zhangsan', 15)
    
    users = query_by_name('zhangsan')
    # result:
    # {'id': 3, 'name': 'zhangsan', 'age': 15}
    
    users = query_by_name2(name='zhangsan')
    # result:
    # {'id': 3, 'name': 'zhangsan', 'age': 15}
    
    users = dbx.select('user.select_all')  
    # result:
    # (3, 'zhangsan', 15)
    # (4, 'lisi', 26)
    # (5, 'wangwu', 38)

    users = dbx.query('user.select_all')
    # result:
    # {'id': 3, 'name': 'zhangsan', 'age': 15}
    # {'id': 4, 'name': 'lisi', 'age': 26}
    # {'id': 5, 'name': 'wangwu', 'age': 38} 
    
    users = dbx.select('user.select_by_name', name='zhangsan')
    # result:
    # (3, 'zhangsan', 15)
    
    # 用db直接执行SQL语句
    users = db.select('select id, name, age from user')
    # result:
    # (3, 'zhangsan', 15)
    # (4, 'lisi', 26)
    # (5, 'wangwu', 38)

    users = db.query('select id, name, age from user')
    # result:
    # {'id': 3, 'name': 'zhangsan', 'age': 15}
    # {'id': 4, 'name': 'lisi', 'age': 26}
    # {'id': 5, 'name': 'wangwu', 'age': 38} 
    
    # where条件用? 占位符，直接传入一一对应的参数就可以了
    users = db.select('select id, name, age from user where name=?', 'zhangsan')
    # result:
    # (3, 'zhangsan', 15)
    
```
db和dbx的select方法返回类型是List[tuple], query方法返回类型是List[Mapping]



##### 事务

```
from mysqlx import trans

# 在方法上声明事务
@trans
def test_transaction():
    insert_func(....)
    update_func(....)

# 在代码块中声明事务
def test_transaction2():
    with trans():
        insert_func(....)
    	update_func(....)
```

单表ORM: https://pypi.org/project/sqlx-orm

如果你要操作PostgreSQL数据库，可以使用相似的框架PgSqlx：https://gitee.com/summry/pgsqlx

如果你要通用的数据库框架，同时支持MySQL和PostgreSQL，可以使用相似的框架mysqlx：https://gitee.com/summry/sqlx-batis
